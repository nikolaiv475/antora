= Antora Pipeline Extensions

Antora provides a lightweight, event-based extension facility that you can tap into to augment or influence the functionality of the site generator.
The extension facility is designed for users of all experience levels.
This page provides a high-level summary of how the extension facility works and introduces the concepts and terminology you need to know to create or use extensions.

== Pipeline steps and events

As described in xref:ROOT:how-antora-works.adoc[], Antora's site generator is a sequence of discrete steps that progressively work to generate a static site, from reading the playbook and gathering the source material to publishing the HTML files and web assets to the output destination.
Antora's event-based extension facility provides a way to hook custom code into this pipeline.

The way the extension facility works is that Antora's pipeline object emits a named event, such as `contentAggregated`, after each step is complete and at other key transition points.
Emitting an event results in function objects called listeners, that are registered with that event, to be called.
To hook your code into the operation of Antora, you register an extension that listens to one or more of these events.

The listeners of an event are called synchronously and in sequence.
Any value returned by the called listeners is ignored.
Once all the listeners for an event have finished running, the Antora pipeline proceeds.

In brief, pipeline extensions register listeners that react to transition events that are raised by the site generator.
Subsequent pages go into detail about how to set up listeners, which events those listeners can observe, and how to access data in the pipeline.

== The Pipeline object

The extension facility in Antora is loosely based on the EventEmitter in Node.js.
The Pipeline object offers an abstraction over this eventing system.
The Pipeline object is responsible for keeping track of event listeners, managing the context variables as they flow through the pipeline, notifying listeners of an event, and providing helpers that make writing extensions easier.
Most of the time, you'll interact with the Pipeline object to bind your event listener using the `on` method.

== What is an event listener?

An [.term]*event listener* provides a way to inject code between any two steps of the site generator.
Technically, an event listener is a callback function that gets invoked (i.e., notified) when an event is raised.
Antora will wait for the listener to run to completion, even if it's async, before proceeding.
A listener can modify the state of in-scope variables in the pipeline, add new variables, or replace existing variables.
More times than not, a listener will add additional files to one or more of the catalogs for processing or publishing.

Currently, listeners of the same event are invoked in sequence to prevent them from interfering with one another.

== What are pipeline variables?

An extension wouldn't be much use if it couldn't access any of the configuration, content, or other data being processed by the generator.
That's where pipeline variables come in.
The [.term]*pipeline variables* provide access to the in-scope objects flowing through the pipeline.
These objects are stored in variables on the pipeline, hence the name pipeline variables.
A listener can pick any of those variables out of the pipeline and work with them.
Basically, a listener can read the same variables that the generator itself can read.
The listener can also push new or replacement variables into the pipeline, so long as the variable is not locked.

== What can an extension do?

To give you an idea of the kinds of extensions you can write and what's possible, here's a glimpse at the things an extension can do to affect the operation of the site generator:

* Modify the configuration
* Change the state of a pipeline variable, such as adding a new file to a catalog
* Push a new variable into the pipeline
* Replace or proxy an exist pipeline variables
* Log messages
* Require user code
* Postprocess content, such as replacing strings in HTML files
* Unpublish files
* Halt the pipeline

In general, an extension taps into the pipeline and tunes its behavior rather than doing a wholesale replacement of it.
There are some limitations with this approach, but it's a trade-off for providing something that's broadly accessible.

== What can't an extension do?

An extension can't change the order of steps the generator performs.
It also can't make the generator skip steps.
It can only get in between two steps and modify the data as it flows through the site generator, which may include introducing new steps.
The most drastic thing an extension can do is to halt the pipeline early.
This action is useful if the work of an extension is complete and no additional processing is necessary.
For example, an extension may only need to report on the content in the site, in which case it can prevent the publishing step from running.
